const { createError } = require('../../helpers');

const { Truck } = require('../../models/truck');

const removeNote = async (req, res) => {
  const { id } = req.params;
  const result = await Truck.findByIdAndRemove(id);
  if (!result || result.assigned_to) {
    throw createError(400);
  }
  res.status(200).json({
    message: 'Truck deleted successfully',
  });
};

module.exports = removeNote;
