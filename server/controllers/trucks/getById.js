const { createError } = require('../../helpers');

const { Truck } = require('../../models/truck');

const getById = async (req, res) => {
  const { id } = req.params;
  const truck = await Truck.findById(id);
  if (!truck) {
    throw createError(404);
  }
  res.json({
    truck,
  });
};

module.exports = getById;
