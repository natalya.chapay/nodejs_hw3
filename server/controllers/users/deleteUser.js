const { User } = require('../../models');

const deleteUser = async (req, res) => {
  const { user } = req;

  await User.deleteOne(user);

  res.json({
    message: 'Profile deleted successfully',
  });
};

module.exports = deleteUser;
