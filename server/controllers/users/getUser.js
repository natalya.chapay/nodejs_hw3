const { User } = require('../../models');

const getUser = async (req, res) => {
  const { user } = req;

  const {
    _id, email, role, created_date,
  } = await User.findOne(user);

  res.json({
    user: {
      _id,
      email,
      role,
      created_date,
    },
  });
};

module.exports = getUser;
