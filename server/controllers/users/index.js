const register = require('../auth/register');
const login = require('../auth/login');
const getUser = require('./getUser');
const updatePassword = require('./updatePassword');
const deleteUser = require('./deleteUser');
const updateAvatar = require('./updateAvatar');

module.exports = {
  register,
  login,
  getUser,
  updatePassword,
  deleteUser,
  updateAvatar,
};
