const jwt = require('jsonwebtoken');
const { createError } = require('../helpers');
const { User } = require('../models');

const { SECRET_KEY } = process.env;

const user = async (req, res, next) => {
  const { authorization = '' } = req.headers;
  const [, token] = authorization.split(' ');

  try {
    const { id } = jwt.verify(token, SECRET_KEY);
    const authUser = await User.findById(id);
    if (!authUser) {
      throw createError(401);
    }
    req.user = authUser;
    next();
  } catch (error) {
    if (error.message === 'Invalid signature') {
      error.status = 401;
    }
    next(error);
  }
};

module.exports = user;
