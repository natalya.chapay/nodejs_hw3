const validation = require('./validation');
const isValidId = require('./isValidId');
const user = require('./user');
const driver = require('./driver');
const shipper = require('./shipper');
const upload = require('./upload');

module.exports = {
  validation,
  isValidId,
  user,
  driver,
  shipper,
  upload,
};
