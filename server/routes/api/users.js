const express = require('express');

const users = require('../../controllers/users');

const { ctrlWrapper } = require('../../helpers');
const { user, upload } = require('../../middlewares');

const router = express.Router();

router.get('/me', user, ctrlWrapper(users.getUser));

router.delete('/me', user, ctrlWrapper(users.deleteUser));

router.patch('/me/password', user, ctrlWrapper(users.updatePassword));

router.patch('/avatars', user, upload.single('avatar'), ctrlWrapper(users.updateAvatar));

module.exports = router;
